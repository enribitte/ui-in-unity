using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Btn_ChangeCharacter : MonoBehaviour
{
    public Sprite CharacterMale;
    public Sprite CharacterFemale;
    private bool IsFemale = false;

    public void ChangeCharacter() 
    {
        if (IsFemale == false)
        {
            gameObject.GetComponent<Image>().sprite = CharacterFemale;

            IsFemale = true;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = CharacterMale;

            IsFemale = false;
        }
    
    
    
    }

    
}
